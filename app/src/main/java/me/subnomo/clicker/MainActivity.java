package me.subnomo.clicker;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
 
        SharedPreferences userPrefs = getSharedPreferences("UserPrefs", 0);
        Boolean firstUse = getSharedPreferences("UserPrefs", 0).getBoolean("firstUse", true);

        if (firstUse) {
            SharedPreferences.Editor editor = userPrefs.edit();
            editor.putBoolean("firstUse", false);
            editor.apply();

            Intent intent = new Intent(this, SettingsActivity.class);
            intent.putExtra("first", true);
            startActivity(intent);
        } else new SwitchSetup(this);

        Button a = (Button) findViewById(R.id.button3);
        Button b = (Button) findViewById(R.id.button2);
        Button c = (Button) findViewById(R.id.button1);

        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SwitchSetup.ready)
                    SwitchSetup.sendSwitch("1XX");
            }
        });

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SwitchSetup.ready)
                    SwitchSetup.sendSwitch("X1X");
            }
        });

        c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SwitchSetup.ready)
                    SwitchSetup.sendSwitch("XX1");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
