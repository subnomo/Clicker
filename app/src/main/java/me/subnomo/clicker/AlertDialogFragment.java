package me.subnomo.clicker;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

public class AlertDialogFragment extends DialogFragment {
    public static String errMsg;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Context context = getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (errMsg != null) {
            builder.setTitle(context.getString(R.string.error_title))
                    .setMessage(errMsg)
                    .setPositiveButton(context.getString(R.string.ok_button), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivity(new Intent(context, SettingsActivity.class));
                        }
                    });
        } else {
            builder.setTitle(context.getString(R.string.error_title))
                    .setMessage(context.getString(R.string.error_message))
                    .setPositiveButton(context.getString(R.string.ok_button), null);
        }

        return builder.create();
    }
}
