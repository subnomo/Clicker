package me.subnomo.clicker;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

public class SwitchSetup {
    public static final String TAG = SwitchSetup.class.getSimpleName();
    public static Context mContext;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final OkHttpClient client = new OkHttpClient();

    // User defined variables
    private static String IP = "192.168.1.227";
    private static String pass = "robots";
    private static String token = "";

    public static boolean ready = false;

    public SwitchSetup(Context context) {
        mContext = context;

        // Load preferences set by user
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        IP = prefs.getString("ip", "192.168.1.227");
        pass = prefs.getString("pass", "robots");
        token = prefs.getString("token", "");

        Log.v(TAG, "IP: " + IP + ", Pass: " + pass);

        auth();
    }

    public void auth() {
        String url = "http://" + IP + ":6326/needAuth";
        Request request = new Request.Builder()
                .url(url)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.e(TAG, "Exception caught: ", e);
                alertUserAboutError(evalErr(e));
            }

            @Override
            public void onResponse(Response response) throws IOException {
                try {
                    if (response.isSuccessful()) {
                        String authNeeded = response.body().string();
                        Log.v(TAG, authNeeded);

                        switch (authNeeded) {
                            case "1":
                                setPass();
                                break;
                            case "2":
                                // Delete any stored token and get new
                                getToken();
                                break;
                            default:
                                ready = true;
                                break;
                        }
                    } else {
                        alertUserAboutError();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Exception caught: ", e);
                    alertUserAboutError(evalErr(e));
                }
            }
        });
    }

    public static void sendSwitch(String command) {
        String url = "http://" + IP + ":6326/switches";
        String json = "{\"code\": \"" + command + "\"," +
                       "\"token\": \"" + token + "\"}";

        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.e(TAG, "Exception caught: ", e);
                alertUserAboutError(evalErr(e));
            }

            @Override
            public void onResponse(Response response) throws IOException {
                try {
                    if (response.isSuccessful()) {
                        // Light up the button or something
                    } else {
                        alertUserAboutError();
                        Log.v(TAG, response.toString());
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception caught: ", e);
                    alertUserAboutError(evalErr(e));
                }
            }
        });
    }

    public void setPass() {
        String url = "http://" + IP + ":6326/newAuth";
        String json = "{\"pass\": \"" + pass + "\"}";

        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.e(TAG, "Exception caught: ", e);
                alertUserAboutError(evalErr(e));
            }

            @Override
            public void onResponse(Response response) throws IOException {
                try {
                    if (response.isSuccessful()) {
                        getToken();
                    } else {
                        alertUserAboutError();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception caught: ", e);
                    alertUserAboutError(evalErr(e));
                }
            }
        });
    }

    public void getToken() {
        String url = "http://" + IP + ":6326/auth";
        String json = "{\"pass\": \"" + pass + "\"}";

        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.e(TAG, "Exception caught: ", e);
            }

            @SuppressLint("CommitPrefEdits")
            @Override
            public void onResponse(Response response) throws IOException {
                try {
                    if (response.isSuccessful()) {
                        token = response.body().string();

                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
                        prefs.edit()
                                .putString("token", token)
                                .commit();

                        auth();
                    } else {
                        alertUserAboutError();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception caught: ", e);
                    alertUserAboutError(evalErr(e));
                }
            }
        });
    }

    public static void alertUserAboutError() {
        final Activity activity = (Activity) mContext;
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(activity.getFragmentManager(), "error_dialog");
    }

    public static void alertUserAboutError(String e) {
        final Activity activity = (Activity) mContext;
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.errMsg = e;
        dialog.show(activity.getFragmentManager(), "error_dialog");
    }

    // Given the error, will return a string describing the error in a user-friendly way
    public static String evalErr(Exception e) {
        String refused = "failed to connect to /192.168.1.1 (port 6326):" +
                " connect failed: ECONNREFUSED (Connection refused)",
            badIP = "failed to connect to /192.168.1.134 (port 6326):" +
                " connect failed: EHOSTUNREACH (No route to host)";

        if (e.getMessage().equals(refused))
            return "The connection was refused! Please re-check the \"IP Address\" setting.";
        else if (e.getMessage().equals(badIP))
            return "A server with that address could not be found. Please re-check the \"IP Address\" setting.";

        return mContext.getString(R.string.error_message);
    }
}
